using Microsoft.EntityFrameworkCore;
using PlatformService.Data;
using AutoMapper;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

Console.WriteLine(">> Using InMem Db");
builder.Services.AddDbContext<AppDbContext>(opt => opt.UseInMemoryDatabase("PlatformInMem"));
builder.Services.AddScoped<IPlatformRepo, PlatformRepo>();
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

PrepDb.PrepPopulation(app, false);

app.Run();
